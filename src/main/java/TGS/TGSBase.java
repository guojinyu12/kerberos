package TGS;

public class TGSBase {
    public static final String[] description = {
            "票据和请求不符，需要服务器的Ticket",// 客户端错误
            "票据和请求不符，需要TGS的Ticket",
            "TGS服务器票据过期",
            "数据源ID与票据不符",
            "数据源IP与票据不符",
            "报文失效",
            "认证成功，但不存在V或V没有认证"
    };
}
