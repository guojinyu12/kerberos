package TGS.TGSClient;

import Base.Ticket.Ticket;
import encrypt.UserDES;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static TGS.TGSClient.TGSBase.description;

public class TGSClientAuthenticator {
    public static Ticket SendRecv(Socket socket, long ID_c, long ID_v, long Kc_tgs, byte[] ticket) throws IOException {
        // 生成报文
        UserDES des = new UserDES(Kc_tgs);// 会话密钥
        // 1) 生成认证并加密
        ByteBuffer auth = ByteBuffer.allocate(3 * 8);
        // 1.1) ID
        auth.putLong(ID_c);
        // 1.2) IP地址
        byte[] ADc = InetAddress.getLocalHost().getAddress();
        System.out.println("IP" + Arrays.toString(ADc));
        auth.put(ADc);
        auth.put(ADc); // 填充对齐
        // 1.3) 时间戳
        auth.putLong(System.currentTimeMillis());
        // 1.4) 加密
        byte[] authenticator = des.encryptionByte(auth.array());
        // 2) 发送报文
        // 发送数据
        DataOutputStream out =
                new DataOutputStream(socket.getOutputStream());
        out.writeLong(ID_v);
        out.write(ticket);
        out.write(authenticator);
        out.flush();
        // 接受响应，读取数据
        DataInputStream in =
                new DataInputStream(socket.getInputStream());
        // 接受服务状态并检测
        long errno;
        if((errno = in.readLong()) != 0b1_0111_1111)
            return new Ticket(errno, description[(int)errno & 0x7f]);
        // 读取报文并解密
        byte[] datum_encrypted = in.readAllBytes();
        byte[] datum = des.decryptionByte(datum_encrypted);
        // 分解报文
        ByteBuffer wrp = ByteBuffer.wrap(datum);
        // 1)会话密钥
        long Kc_v = wrp.getLong();
        // 2)票据对象并检测
        long ID_v_copy = wrp.getLong();
        if(ID_v_copy != ID_v)
            return new Ticket(0b1_0000_000, description[0]);
        // 3)发票时间
        long timeStamp = wrp.getLong();
        // 4)票据
        byte[] ticket_v = new byte[6 * 8];
        wrp.get(ticket_v);
        System.out.println("V的票据：" + Arrays.toString(ticket_v));
        // 函数结束
        return new Ticket(Kc_v, timeStamp, -1, ticket_v);
    }
}
