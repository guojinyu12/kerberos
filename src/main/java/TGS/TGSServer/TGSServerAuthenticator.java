package TGS.TGSServer;

import Base.ID.IDBase;
import encrypt.DESKeyBuilder;
import encrypt.UserDES;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

import static TGS.TGSClient.TGSBase.description;

public class TGSServerAuthenticator implements Runnable{
    final static int interval = 2 * 1000;
    static ConcurrentHashMap<Long, Long> table = new ConcurrentHashMap<>();
    Socket socket;
    long ID_tgs;
    long K_tgs;
    long lifetime;
    public TGSServerAuthenticator(Socket socket, long ID_tgs, long K_tgs){
        long ID_v = IDBase.translate("ChatServer");
        table.put(ID_v, 0L);
        this.socket = socket;
        this.ID_tgs = ID_tgs;
        this.K_tgs = K_tgs;
        this.lifetime = 10 * 60 * 1000;
    }

    public TGSServerAuthenticator( Socket socket, long ID_tgs,
            long K_tgs, long lifetime){
        this.socket = socket;
        this.ID_tgs = ID_tgs;
        this.K_tgs = K_tgs;
        this.lifetime = lifetime;
    }
    @Override
    public void run(){
        try {
            recvSend();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
    public void recvSend()throws IOException {
        DataInputStream in =
                new DataInputStream(socket.getInputStream());
        DataOutputStream out =
                new DataOutputStream(socket.getOutputStream());
        // 读取报文
        long ID_v = in.readLong();
        System.out.printf("IDv = %x%n", ID_v);
        byte[] ticket1 = new byte[48];
        int num ;
        num = in.read(ticket1);
        // 读取字节数检查
        if(num < 48)
            return;
        // 解密票据
        UserDES DES_tgs = new UserDES(K_tgs);
        ticket1 = DES_tgs.decryptionByte(ticket1);
        System.out.println("TGSAuthenticator:64:TGS票据" + Arrays.toString(ticket1));
        // 包装字节数组便于处理
        ByteBuffer wrp;
        wrp = ByteBuffer.wrap(ticket1);
        // 依次读取次Kc_tgs、IDc、ADc|ADc、ID_tgs、timestamp、lifetime，
        // 并保存为datum数组前六个元素成员
        long[] datum = new long[9];
        for(int i = 0; i < 6; i++)
            datum[i] = wrp.getLong();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.printf("TGSAuthenticator:74:会话密钥：%d、源ID：%x、源IP：%x%n票据ID：%x、时间戳：%s、生命周期：%d%n",
                datum[0], datum[1], (int)datum[2], datum[3], sf.format(datum[4]), datum[5]);
        {
            // 第一次检查
            // 检查是否是属于本服务器的票据
            if (datum[3] != ID_tgs){
                out.writeLong(0b1_0000_0001);
                System.out.printf("错误:本机ID：%x 票据ID：%x 问题描述：%s%n",
                        datum[3], ID_tgs, description[1]);
                return;
            }
            // 检测票据是否过期
            if (datum[4] + datum[5] < System.currentTimeMillis()){
                // 删除过期的会话密钥，ID-密钥映射
                table.remove(datum[1], datum[0]);
                out.writeLong(0b1_0000_0010);
                System.out.println(description[2]);
                return;
            }
        }
        // 会话密钥, L_c_tgs
        UserDES DES_c_tgs = new UserDES(datum[0]);
        byte[] authenticator = new byte[24];
        num = in.read(authenticator);
        // 读取字节数检查
        if(num < 24)
            return;
        authenticator = DES_c_tgs.decryptionByte(authenticator);
        wrp = ByteBuffer.wrap(authenticator);
        // 依次读取次IDc、ADc|ADc、timestamp4，并保存为datum数组后三个元素成员
        // 并放入datum数组中
        for(int i = 6; i < 9; i++)
            datum[i] = wrp.getLong();
        System.out.printf("TGSAuthenticator.java:107:源ID：%x、源IP：%x、时间戳：%s%n",
                datum[6], datum[7], sf.format(datum[8]));
        {
            // 第二次检查
            // 认证数据数据源
            if (datum[1] != datum[6]){
                out.writeLong(0b1_0000_0011);
                System.out.println(description[3]);
                return;
            }
            // 检查地址是否一致
            if (datum[2] != datum[7]){
                out.writeLong(0b1_0000_0100);
                System.out.println(description[4]);
                return;
            }
            // 报文时效性检查
            if (interval < System.currentTimeMillis() - datum[8]){
                out.writeLong(0b1_0000_0101);
                System.out.println(description[5]);
                return;
            }
        }

        // 生成票据
        ByteBuffer buffer = ByteBuffer.allocate(6 * 8);
        long Kc_v = new DESKeyBuilder().getKey();
        buffer.putLong(Kc_v);
        buffer.putLong(datum[6]); // ID_c
        buffer.putLong(datum[7]); // ADc|ADc，第二个用于字节对齐
        buffer.putLong(ID_v);
        long timestamp4 = System.currentTimeMillis();
        buffer.putLong(timestamp4);
        buffer.putLong(lifetime);
        Long K_v = table.get(ID_v);
        // table.put(ID_c, K_c_tgs)
        table.put(datum[1], datum[0]);// 避免自身映射
        System.out.println("ID-密钥映射表" + table);
        if(K_v == null){
            out.writeLong(0b1_0000_0110);
            System.out.println(description[6]);
            return;
        }
        byte[] ticket_v =
                new UserDES(K_v).encryptionByte(buffer.array());
        System.out.println("TGSAuthenticator.java:151:" + Arrays.toString(ticket_v));
        ByteBuffer buffer1 = ByteBuffer.allocate(3 * 8 + 6 * 8);
        buffer1.putLong(Kc_v);
        buffer1.putLong(ID_v);
        buffer1.putLong(timestamp4);
        buffer1.put(ticket_v);
        // datum[0]是K_c_tgs
        byte[] result =
                new UserDES(datum[0]).encryptionByte(buffer1.array());
        out.writeLong(0b1_0111_1111);
        out.write(result);
        out.flush();
        socket.close();
    }
}
