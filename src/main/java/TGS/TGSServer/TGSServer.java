package TGS.TGSServer;


import java.io.File;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Base.ID.IDBase;

public class TGSServer {
    static final int nThreads = 4;
    static final String start = "开始监听";
    static final String accept = "一次新的TGS认证过程";
    public static void main(String[] args)throws Exception{
        long ID_tgs = IDBase.translate("TGS");
        int port = 12001;
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println(start);
        ExecutorService poolExecutor =
                Executors.newFixedThreadPool(nThreads);

        for(int i = 0; i < 100; i++) {
            Socket socket = serverSocket.accept();
            System.out.println(accept);
            poolExecutor.submit(new TGSServerAuthenticator(socket, ID_tgs, 0));
        }
    }
}
