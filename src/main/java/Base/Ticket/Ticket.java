package Base.Ticket;

// 时间戳不小于零时，是包含DES密钥的票据类；小于零时，是错误信息。
public class Ticket {
    public long K;
    public long timeStamp;
    public long lifetime;
    public byte[] ticket;
    public Ticket(long K, long timeStamp, long lifetime, byte[] ticket){
        this.K = K;
        this.timeStamp = timeStamp;
        this.lifetime = lifetime;
        this.ticket = ticket;
    }
    public Ticket(long errno, String description){
        this.K = errno;
        this.timeStamp = -1;
        this.lifetime = -1;
        this.ticket = description.getBytes();
    }
    public Ticket(long errno, byte[] description){
        this.K = errno;
        this.timeStamp = -1;
        this.lifetime = -1;
        this.ticket = description;
    }
}
