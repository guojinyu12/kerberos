package Base.ID;

import java.nio.ByteBuffer;

public class IDBase {
    public static long translate(String ID){
        ByteBuffer id = ByteBuffer.wrap(ID.getBytes());
        long ID_C = 0;
        while(id.remaining() >= 8){
            ID_C ^= id.getLong();
        }
        if(id.hasRemaining()){
            ByteBuffer id_aux = ByteBuffer.allocate(8);
            id_aux.put(id);
            while (id_aux.hasRemaining()){
                id_aux.put((byte)0);
            }
            id_aux.flip();
            ID_C ^= id_aux.getLong();
        }
        return ID_C;
    }
}
