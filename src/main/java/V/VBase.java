package V;

public class VBase {
    public static final String[] description = {
            "时间戳认证失败", // 客户端错误
            "票据和请求不符，需要V的Ticket",
            "服务器票据过期",
            "数据源ID与票据不符",
            "数据源IP与票据不符",
            "报文失效",
    };
}
