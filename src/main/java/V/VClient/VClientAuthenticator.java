package V.VClient;

import Base.Ticket.Ticket;
import encrypt.DES16;
import encrypt.UserDES;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.rmi.Remote;
import java.text.SimpleDateFormat;

public class VClientAuthenticator implements Remote{
    public static final String[] description = {
            "时间戳认证失败", // 客户端错误
            "票据和请求不符，需要V的Ticket",
            "服务器票据过期",
            "数据源ID与票据不符",
            "数据源IP与票据不符",
            "报文失效",
    };
    long timeStamp;
    // Ticket or Error
    public byte[] send(long ID_c, byte[] ticket, long K)throws UnknownHostException {
        // 生成报文
        UserDES des = new UserDES(K);// 会话密钥
        // 1) 生成认证并加密
        ByteBuffer auth = ByteBuffer.allocate(3 * 8);
        // 1.1) ID
        auth.putLong(ID_c);
        // 1.2) IP地址
        byte[] ADc = InetAddress.getLocalHost().getAddress();
        auth.put(ADc);
        auth.put(ADc); // 填充对齐
        // 1.3) 时间戳
        timeStamp = System.currentTimeMillis();
        auth.putLong(timeStamp);
        // 1.4) 加密
        byte[] authenticator = des.encryptionByte(auth.array());
        // 报文
        ByteBuffer pkg = ByteBuffer.allocate(ticket.length + authenticator.length);
        pkg.put(ticket);
        pkg.put(authenticator);
        return pkg.array();
    }
    public Ticket receive(long errno, long K){
        errno = new DES16(K).decrypt(errno);
        if(errno <= 0b11_111_1111 && errno > 0){
            System.out.println(errno);
            return new Ticket(errno, description[(int)errno & 0x7f]);
        }
        var sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
        System.out.println(sf.format(timeStamp) +
                "\n" + sf.format(errno - 1)
                + "\n" + timeStamp + "\n" + (errno - 1));
        return errno - 1 != this.timeStamp ?
        new Ticket(0b11_000_0101, description[0]) :
        new Ticket(0b11_111_1111, errno - 1, 0, null);
    }
}
