package V.VServer;

import encrypt.UserDES;
import encrypt.DES16;

import java.nio.ByteBuffer;
import java.rmi.Remote;
import java.text.SimpleDateFormat;

import static V.VBase.description;

public class VServerAuthenticator implements Remote {
    final static long interval = 2 * 1000;
    public static class VResult{
        public long errno;
    }
    public static class FailResult extends VResult{
        public FailResult(long errno){
            this.errno = errno;
        }
    }
    public static class SuccessResult extends VResult{
        public long timeStamp;
        public long K;
        public SuccessResult(long errno, long ts, long K){
            this.errno = errno;
            this.timeStamp = ts;
            this.K = K;
        }
    }

    public VResult unpack(byte[] pkg, long ID_v, long Kv_tgs) {
        ByteBuffer pkgBuffer = ByteBuffer.wrap(pkg);
        // 1) 解包票据
        byte[] ticket = new byte[48];
        pkgBuffer.get(ticket);
        ticket = new UserDES(Kv_tgs).decryptionByte(ticket);
        // 包装字节数组便于处理
        ByteBuffer wrp;
        wrp = ByteBuffer.wrap(ticket);
        // 依次读取次Kc_tgs、IDc、ADc|ADc、ID_tgs、timestamp、lifetime，
        // 并保存为datum数组前六个元素成员
        long[] datum = new long[9];
        for (int i = 0; i < 6; i++)
            datum[i] = wrp.getLong();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.printf("TGSAuthenticator:74:会话密钥：%d、源ID：%x、源IP：%x%n票据ID：%x、时间戳：%s、生命周期：%d%n",
                datum[0], datum[1], (int) datum[2], datum[3], sf.format(datum[4]), datum[5]);
        {
            // 第一次检查
            // 检查是否是属于本服务器的票据
            if (datum[3] != ID_v) {
                System.out.printf("错误:本机ID：%x 票据ID：%x 问题描述：%s%n",
                        datum[3], ID_v, description[1]);
                return new FailResult(0b11_000_0001);
            }
            // 检测票据是否过期
            if (datum[4] + datum[5] < System.currentTimeMillis()) {
                System.out.println(description[2]);
                return new FailResult(0b11_000_0010);
            }
        }
        // 会话密钥, L_c_v
        UserDES DES_c_v = new UserDES(datum[0]);
        // 2) 解包认证
        byte[] authenticator = new byte[24];
        pkgBuffer.get(authenticator);
        authenticator = DES_c_v.decryptionByte(authenticator);
        wrp = ByteBuffer.wrap(authenticator);
        // 依次读取次IDc、ADc|ADc、timestamp4，并保存为datum数组后三个元素成员
        // 并放入datum数组中
        for (int i = 6; i < 9; i++)
            datum[i] = wrp.getLong();
        System.out.printf("TGSAuthenticator.java:107:源ID：%x、源IP：%x、时间戳：%s%n",
                datum[6], datum[7], sf.format(datum[8]));
        {
            // 第二次检查
            // 认证数据数据源
            if (datum[1] != datum[6]) {
                // out.writeLong(0b1_0000_0011);
                System.out.println(description[3]);
                return new FailResult(0b11_000_0011);
            }
            // 检查地址是否一致
            if (datum[2] != datum[7]) {
                System.out.println(description[4]);
                return new FailResult(0b11_000_0100);
            }
            // 报文时效性检查
            if (interval < System.currentTimeMillis() - datum[8]) {
                System.out.println(description[5]);
                return new FailResult(0b11_000_0101);
            }
        }
        return new SuccessResult(0b11_111_1111, datum[8], datum[0]);
    }
    public static byte[] pack(VResult res, long K){
        ByteBuffer buffer = ByteBuffer.allocate(2 * 8);
        buffer.putLong(res.errno);
        if(res.errno == 0b11_111_1111){
            long encrypted =
                    new DES16(K).encrypt(((SuccessResult)res).timeStamp + 1);
            buffer.putLong(encrypted);
        }
        return buffer.array();
    }
}
