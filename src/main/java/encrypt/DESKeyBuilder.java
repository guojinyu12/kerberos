package encrypt;

import java.util.Random;

public class DESKeyBuilder {
    long key;
    static long check_aux(long x){
        // evaluate the number of 1 per 8 bit
        x = x - ((x >>> 1) & 0x5555_5555_5555_5555L);
        x = ((x >>> 2) & 0x3333_3333_3333_3333L) + (x & 0x3333_3333_3333_3333L);
        x = ((x >>> 4) + x) & 0x0f0f_0f0f_0f0f_0f0fL;
        return x;
    }
    public long getKey(){
        final Random random = new Random();
        key = random.nextLong();
        key &= 0x7f7f_7f7f_7f7f_7f7fL;  // spare check bit
        long copy = check_aux(key);
        // check bit
        if ((copy & 0xffL) % 2 == 0) key |= 0x80L;
        copy >>>= 8;
        if ((copy & 0xffL) % 2 == 0) key |= 0x8000L;
        copy >>>= 8;
        if ((copy & 0xffL) % 2 == 0) key |= 0x8000_00L;
        copy >>>= 8;
        if ((copy & 0xffL) % 2 == 0) key |= 0x8000_0000L;
        copy >>>= 8;
        if ((copy & 0xffL) % 2 == 0) key |= 0x8000_0000_00L;
        copy >>>= 8;
        if ((copy & 0xffL) % 2 == 0) key |= 0x8000_0000_0000L;
        copy >>>= 8;
        if ((copy & 0xffL) % 2 == 0) key |= 0x8000_0000_0000_00L;
        copy >>>= 8;
        if ((copy & 0xffL) % 2 == 0) key |= 0x8000_0000_0000_0000L;
        return key;
    }
    public long setKey(long k){
        return key = k;
    }
    public boolean check(){
        long copy = check_aux(key);
        for(int i = 0; i < 8; i ++){
            if((copy & 0xffL) % 2 == 0)
                return false;
            copy >>>= 8;
        }
        return true;
    }
}
