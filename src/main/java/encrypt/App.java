package encrypt;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        String str = "A mutable sequence of characters. This class provides an API compatible with StringBuffer, but with no guarantee of synchronization. This class is designed for use as a drop-in replacement for StringBuffer in places where the string buffer was being used by a single thread (as is generally the case). Where possible, it is recommended that this class be used in preference to StringBuffer as it will be faster under most implementations."
+ "The principal operations on a StringBuilder are the append and insert methods, which are overloaded so as to accept "
                +"data of any type. Each effectively converts a given datum to a string and then appends or inserts the"
                +" characters of that string to the string builder. The append method always adds these characters at the end of the builder; the insert method adds the characters at a specified point."
+ "For example, if z refers to a string builder object whose current contents are \"start\", then the method call z.append(\"le\") would cause the string builder to contain startle, whereas z.insert(4, le) would alter the string builder to contain starlet."
+ "In general, if sb refers to an instance of a StringBuilder, then sb.append(x) has the same effect as sb.insert(sb.length(), x). Every string builder has a capacity. As long as the length of the character sequence contained in the string builder does not exceed the capacity, it is not necessary to allocate a new internal buffer. If the internal buffer overflows, it is automatically made larger."
+ "Instances of StringBuilder are not safe for use by multiple threads. If such synchronization is required then it is recommended that StringBuffer be used."
        +"又到一年高考时，记者2日从有关部门获悉，今年全国高考报名1078万人，再创历史新高，考场、考务人员数量相应增加。(记者胡浩，海报设计贾伊宁)";
        // UserDES2 des2 = new UserDES2( 0x7a45_25bd_3471_efa2L);
        // String c = des2.encrypt(str);
        // String str2 = des2.decrypt(c);
        // System.out.println(str);
        // System.out.println(c);
        // System.out.println(str2);
        // ByteBuffer buffer = ByteBuffer.allocate(8);
        // buffer.putLong(-1);
        // buffer.flip();
        // System.out.println(buffer.getLong());

        // DESKeyBuilder desKeyBuilder = new DESKeyBuilder();
        // long key = desKeyBuilder.getKey();
        // long x = DESKeyBuilder.check_aux(key);
        // for(int i = 0; i < 8; i++){
        //     System.out.println(x & 0xff);
        //     x >>>= 8;
        // }

    }
}
