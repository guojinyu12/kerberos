package encrypt;
import java.util.Arrays;

public class UserDES {
    private final DES16 des;
    private long key64;
    private long byte2long(byte[] bytes, int i){
        return ((long)bytes[i] & 0xff) << 56 | ((long)bytes[i + 1] & 0xff) << 48 | ((long)bytes[i + 2] & 0xff) << 40 | ((long)bytes[i + 3] & 0xff) << 32 |
                ((long)bytes[i + 4] & 0xff) << 24 |  ((long)bytes[i + 5] & 0xff) << 16 | ((long)bytes[i + 6] & 0xff) << 8 |  ((long)bytes[i + 7] & 0xff);
    }
    private void long2byte(byte[] bytes, int i, long x){
        bytes[i] = (byte) (x >>> 56 & 0xff);
        bytes[i + 1] = (byte) (x >>> 48 & 0xff);
        bytes[i + 2] = (byte) (x >>> 40 & 0xff);
        bytes[i + 3] = (byte) (x >>> 32 & 0xff);
        bytes[i + 4] = (byte) (x >>> 24 & 0xff);
        bytes[i + 5] = (byte) (x >>> 16 & 0xff);
        bytes[i + 6] = (byte) (x >>> 8 & 0xff);
        bytes[i + 7] = (byte) (x & 0xff);
    }
    public UserDES(long key64){
        this.key64 = key64;
        des = new DES16(key64);
    }
    public byte[] encryption(String m){
        return encryptionByte(m.getBytes());
    }
    public byte[] encryptionByte(byte[] m){
        byte[] result = new byte[(((m.length + 7) >> 3) << 3)];
        for(int i = (m.length / 8 - 1) * 8; i >= 0; i -= 8){
            long2byte(result, i, des.encrypt(byte2long(m, i)));
        }
        // result.length must be the multiple of 8
        // using 0xff fill the rest
        if(result.length > m.length){
            for(int i = result.length - 8; i < result.length; i++){
                result[i] = i < m.length ? m[i] : (byte) 0xff;
            }
            final int i = result.length - 8;
            long2byte(result, i, des.encrypt(byte2long(result, i)));
        }
        return result;
    }
    public byte[] decryptionByte(byte[] c){
        byte[] result = new byte[c.length];
        for(int i = (c.length / 8 - 1) * 8; i >= 0; i -= 8){
            long2byte(result, i, des.decrypt(byte2long(c, i)));
        }
        // result contains 0xff
        return result;
    }
    public String decryption(byte[] c){
        byte[] result = decryptionByte(c);
        int length = result.length;
        while (result[length - 1] == (byte) 0xff){
            length --;
        }
        return new String(result, 0, length);
    }
    public String encrypt(String m){
        byte[] c = encryption(m);
        return Arrays.toString(c);
    }
    public String decrypt(String c){
        String[] c1 = c.substring(1, c.length() - 1).split(", ");
        byte[] c2 = new byte[c1.length];
        for(int i = 0; i < c1.length; i ++){
            c2[i] = Byte.parseByte(c1[i]);
        }
        return decryption(c2);
    }
    public long getKey64(){
        return key64;
    }
    public void setKey64(long key64){
        this.key64 = key64;
        des.setNewKey(key64);
    }
}