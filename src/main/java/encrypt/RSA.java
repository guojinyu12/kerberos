package encrypt;

import java.math.BigInteger;
import java.util.Random;

import static java.lang.Byte.SIZE;

public class RSA{
    BigInteger publicKey;
    BigInteger privateKey;
    BigInteger e;
    int length;
    // φ(n)
    BigInteger[] gcdEx(BigInteger a, BigInteger b){
        if(b.signum() == 0){
            return new BigInteger[]{BigInteger.ONE, BigInteger.ZERO, a};
        }
        BigInteger[] qr = a.divideAndRemainder(b);
        BigInteger[] res = gcdEx(b, qr[1]);
        BigInteger x = res[0];
        res[0] = res[1];
        res[1] = x.subtract(qr[0].multiply(res[1]));
        return res;
    }
    void evaluateD(BigInteger PhiN){
        BigInteger x = gcdEx(e, PhiN)[0];
        privateKey = x.signum() < 0 ? x.add(PhiN) : x;
    }
    public static RSA getNewInstance(){
        return new RSA(2 << 16 + 1);
    }
    public RSA(){
        this(2 << 16 + 1, 2048);
    }
    public RSA(long e){
        this.e = BigInteger.valueOf(e);
    }
    public RSA(BigInteger n){
        this((1<<16) + 1, n); // 2^16 + 1
    }
    public RSA(long e, int bitLength){
        this.e = BigInteger.valueOf(e);
        this.length = (bitLength + SIZE - 1) / SIZE;
        // bitLength / 2 + (bitLength + 1) / 2 == bitLength
        Random x = new Random(System.currentTimeMillis());
        BigInteger p = BigInteger.probablePrime(bitLength / 2, x);
        BigInteger q = BigInteger.probablePrime((bitLength + 1) / 2 - 1, x);
        this.publicKey = p.multiply(q);
        evaluateD(p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE)));
        // evaluate d
    }
    public RSA(long e, BigInteger n){
        this.e = BigInteger.valueOf(e);
        this.length = (n.bitLength() + SIZE - 1) / SIZE;
        this.publicKey = n;
    }
    public void setPublicKey(String publicKey){
        this.publicKey = new BigInteger(publicKey);
    }
    public void setPrivateKey(String privateKey){
        this.privateKey = new BigInteger(privateKey);
    }
    public void setPublicKey(long e, BigInteger publicKey){
        this.e = BigInteger.valueOf(e);
        this.publicKey = publicKey;
    }
    public void setPrivateKey(long e, BigInteger privateKey){
        this.e = BigInteger.valueOf(e);
        this.privateKey = privateKey;
    }
    BigInteger modPow(BigInteger m, BigInteger pow, BigInteger n){
        BigInteger rem = m;
        BigInteger result = BigInteger.ONE;
        int i = 0;
        int length = pow.bitLength();
        while (true){
            if(pow.testBit(i++)){
                result = result.multiply(rem).remainder(n);
            }
            if(i >= length) {
                break;
            }
            rem = rem.multiply(rem).remainder(n);
        }
        return result;
    }
    public BigInteger PublicEncrypt(BigInteger BigM){
        return BigM.modPow(e, publicKey);
    }
    public BigInteger PrivateDecrypt(BigInteger BigM){
        return BigM.modPow(privateKey, publicKey);
    }
    public BigInteger PrivateEncrypt(BigInteger BigM){
        return BigM.modPow(privateKey, publicKey);
    }
    public BigInteger PublicDecrypt(BigInteger BigM){
        return BigM.modPow(e, publicKey);
    }
}