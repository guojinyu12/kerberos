package encrypt;

import java.nio.ByteBuffer;

public class UserDES2 {
    private final DES16 des;
    private long key64;
    public UserDES2(long key64){
        this.key64 = key64;
        des = new DES16(key64);
    }
    public byte[] encryption(String m){
        return encryptionByte(m.getBytes());
    }
    public byte[] encryptionByte(byte[] m){
        ByteBuffer resultWarp = ByteBuffer.allocate((m.length + 7) & 0xefff_fff8);
        ByteBuffer mWarp = ByteBuffer.wrap(m);
        while (mWarp.remaining() >= 8){
            long y = des.encrypt(mWarp.getLong());
            resultWarp.putLong(y);
        }
        // result.length must be the multiple of 8
        // using 0xff fill the rest
        if(mWarp.hasRemaining()){
            final int n = 8 - mWarp.remaining();
            ByteBuffer aux = ByteBuffer.allocate(8);
            aux.put(mWarp);
            for (int i = 0; i < n; i++){
                aux.put((byte)0xff);
            }
            aux.flip();
            resultWarp.putLong(des.encrypt(aux.getLong()));
        }
        return resultWarp.array();
    }
    public byte[] decryptionByte(byte[] c){
        ByteBuffer result = ByteBuffer.allocate(c.length);
        ByteBuffer cWarp = ByteBuffer.wrap(c);
        while (cWarp.remaining() >= 8)
            result.putLong(des.decrypt(cWarp.getLong()));
        // result contains 0xff
        return result.array();
    }
    public String decryption(byte[] c){
        byte[] result = decryptionByte(c);
        int length = result.length;
        while (result[length - 1] == (byte) 0xff){
            length --;
        }
        return new String(result, 0, length);
    }
    public String encrypt(String m){
        byte[] c = encryption(m);
        StringBuilder result = new StringBuilder(c.length * 2);
        for (byte b : c) {
            final String t = Integer.toString(b & 0xff, 16);
            result.append(t.length() == 2 ? t : "0" + t);
        }
        return result.toString();
    }
    public String decrypt(String c){
        byte[] c2 = new byte[c.length() / 2];
        for(int i = 0, n = c2.length; i < n; i ++){
            c2[i] = (byte)Integer.parseInt(c.substring(i * 2, i * 2 + 2), 16);
        }
        return decryption(c2);
    }
    public long getKey64(){
        return key64;
    }
    public void setKey64(long key64){
        this.key64 = key64;
        des.setNewKey(key64);
    }
}
