package encrypt;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class UserRSA extends RSA{
    public static UserRSA GetNewInstance(){ return new UserRSA(2 << 16 + 1); }
    public UserRSA(){ super(); }
    public UserRSA(long e, int bitLength){ super(e, bitLength); }
    public UserRSA(BigInteger n){ super(n); }
    public UserRSA(long e, BigInteger n){ super(e, n); }
    public UserRSA(long e){ super(e); }

    @Override
    public void setPrivateKey(String privateKey) {
        super.setPrivateKey(privateKey);
    }

    @Override
    public void setPublicKey(String publicKey) {
        super.setPublicKey(publicKey);
    }

    @Override
    public void setPrivateKey(long e, BigInteger privateKey) {
        super.setPrivateKey(e, privateKey);
    }

    @Override
    public void setPublicKey(long e, BigInteger publicKey) {
        super.setPublicKey(e, publicKey);
    }

    public byte[] PublicEncrypt(byte[] m) {
        final int length = this.length;
        final int mlength = m.length;
        final int partLength = this.length - 1;
        byte[] part = new byte[length];// avoid that m is too long to encrypt
        part[0] = 0;
        byte[] result = new byte[(mlength + partLength - 1) / partLength * length];
        int i = 0, j = 0;
        {
            int len;
            if ((len = mlength % partLength) > 0) {
                System.arraycopy(m, 0, part, length - len, len);
                // padding
                for(int k = length - len - 1; k > 0; k--)
                    part[k] = (byte)0xff;
                BigInteger bigM = new BigInteger(part);
                bigM = super.PublicEncrypt(bigM);
                byte[] resultpart = bigM.toByteArray();
                System.arraycopy(resultpart,
                        0,
                        result,
                        length - resultpart.length,
                        resultpart.length);
                i = len;
                j = length;
            }
        }
        for (;i < mlength; i += partLength, j += length){
            System.arraycopy(m, i, part, 1, partLength);
            BigInteger bigM = new BigInteger(part);
            bigM = super.PublicEncrypt(bigM);
            byte[] resultpart = bigM.toByteArray();
            System.arraycopy(resultpart,
                    0,
                    result,
                    j + length - resultpart.length,
                    resultpart.length);

        }
        return result;
    }
    public byte[] PrivateDecrypt(byte[] c){
        final int length = this.length;
        final int clength = c.length;
        byte[] part = new byte[length];
        byte[] result = new byte[clength / length * (length - 1)];

        for (int i = 0, j = 0;i < clength; i += length, j += length - 1){
            System.arraycopy(c, i, part, 0, length);
            BigInteger bigM = new BigInteger(part);
            bigM = super.PrivateDecrypt(bigM);
            byte[] resultpart = bigM.toByteArray();

            System.arraycopy(resultpart,
                    1,
                    result,
                    j + length - resultpart.length,
                    resultpart.length - 1);
        }
        return result;
    }
    public byte[] PrivateEncrypt(byte[] m) {
        final int length = this.length;
        final int mlength = m.length;
        final int partLength = this.length - 1;
        byte[] part = new byte[length];// avoid that m is too long to encrypt
        part[0] = 0;
        byte[] result = new byte[(mlength + partLength - 1) / partLength * length];
        int i = 0, j = 0;
        {
            int len;
            if ((len = mlength % partLength) > 0) {
                System.arraycopy(m, 0, part, length - len, len);
                // padding
                for(int k = length - len - 1; k > 0; k--)
                    part[k] = (byte)0xff;
                BigInteger bigM = new BigInteger(part);
                bigM = super.PrivateEncrypt(bigM);
                byte[] resultpart = bigM.toByteArray();
                System.arraycopy(resultpart,
                        0,
                        result,
                        length - resultpart.length,
                        resultpart.length);
                i = len;
                j = length;
            }
        }
        for (;i < mlength; i += partLength, j += length){
            System.arraycopy(m, i, part, 1, partLength);
            BigInteger bigM = new BigInteger(part);
            bigM = super.PrivateEncrypt(bigM);
            byte[] resultpart = bigM.toByteArray();
            System.arraycopy(resultpart,
                    0,
                    result,
                    j + length - resultpart.length,
                    resultpart.length);

        }
        return result;
    }
    public byte[] PublicDecrypt(byte[] c){
        final int length = this.length;
        final int clength = c.length;
        byte[] part = new byte[length];
        byte[] result = new byte[clength / length * (length - 1)];

        for (int i = 0, j = 0;i < clength; i += length, j += length - 1){
            System.arraycopy(c, i, part, 0, length);
            BigInteger bigM = new BigInteger(part);
            bigM = super.PublicDecrypt(bigM);
            byte[] resultpart = bigM.toByteArray();

            System.arraycopy(resultpart,
                    1,
                    result,
                    j + length - resultpart.length,
                    resultpart.length - 1);
        }
        return result;
    }
    public String PublicEncrypt(String m){
        return Arrays.toString(PublicEncrypt(m.getBytes(StandardCharsets.UTF_8)));
    }
    public String PrivateEncrypt(String m){
        return Arrays.toString(PublicEncrypt(m.getBytes(StandardCharsets.UTF_8)));
    }
    public String PublicDecrypt(String c){
        String[] c1 = c.substring(1, c.length() - 1).split(", ");
        byte[] c2 = new byte[c1.length];
        for(int i = 0; i < c1.length; i ++){
            c2[i] = Byte.parseByte(c1[i]);
        }
        c2 = PublicDecrypt(c2);
        int i = 0;
        while (c2[i] == (byte)0xff){
            i++;
        }
        return new String(c2, i, c2.length - i);

    }
    public String PrivateDecrypt(String c){
        String[] c1 = c.substring(1, c.length() - 1).split(", ");
        byte[] c2 = new byte[c1.length];
        for(int i = 0; i < c1.length; i ++){
            c2[i] = Byte.parseByte(c1[i]);
        }
        c2 = PrivateDecrypt(c2);
        int i = 0;
        while (c2[i] == (byte)0xff){
            i++;
        }
        return new String(c2, i, c2.length - i);
    }

}
