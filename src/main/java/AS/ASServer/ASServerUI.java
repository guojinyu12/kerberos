package AS.ASServer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ASServerUI extends JFrame {
    static final String start = "开始监听\n";
    static final String accept = "连接建立\n";
    public static void main(String[] argv){
        new ASServerUI();
    }
    final JToolBar toolBar;
    final JTextArea text;

    public ASServerUI(){
        setTitle("AS服务器窗口");
        // set size and enable resize
        setSize(500, 500);
        setResizable(true);
        // 添加按钮
        toolBar = new JToolBar();
        JButton start = new JButton();
        // 事件
        start.setAction(new AbstractAction("启动") {
            @Override
            public void actionPerformed(ActionEvent e) {
                text.append("Hello world!!\n");
            }
        });
        // 添加按钮
        toolBar.add(start);
        JButton stop = new JButton();
        stop.setAction(new AbstractAction("停止") {
            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
            }
        });
        toolBar.add(stop);
        // text area
        text = new JTextArea();
        text.setLineWrap(true); // auto newline
        text.setEditable(false);// only print
        // add to user interface
        final Container contentPane = getContentPane();
        contentPane.add(toolBar, BorderLayout.NORTH);
        contentPane.add(new JScrollPane(text), BorderLayout.CENTER);
        // show the window
        setVisible(true);
        // exit
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
